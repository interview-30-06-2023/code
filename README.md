# Website

The repository contains HTML code which is used to serve a static website.

## Deployment

Any changes to the code base will initially trigger tests using the htmlproofer tool. If the job succeeds and the change is on the main branch, the deployment will be triggered using the AWS CLI. The local files will be synced with a remote S3 bucket.

